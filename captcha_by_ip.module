<?php
// $Id$

/**
 * @file
 * Module file for captcha_by_ip. This module provides the ability
 * to determine whether to show a CAPTCHA based on IP.
 */

/**
 * Implementation of hook_perm().
 */
function captcha_by_ip_perm() {
  return array('administer captcha by ip');
}

/**
 * Implementation of hook_menu().
 */
function captcha_by_ip_menu() {
  $items = array();
  $items['admin/user/captcha/captcha_by_ip'] = array(
    'title' => 'CAPTCHA By IP',
    'description' => 'Settings for Captcha By IP module',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('captcha_by_ip_settings_form'),
    'access arguments' => array('administer captcha by ip'),
    'type' => MENU_NORMAL_ITEM,
  );
  return $items;
}

/**
 * Implementation of hook_theme().
 */
function captcha_by_ip_theme() {
  return array(
    'captcha_by_ip_admin_settings_overview' => array(
      'arguments' => array('form' => NULL),
    ),
  );
}

/**
 * Settings form for Captcha By IP.
 */
function captcha_by_ip_settings_form($form_state) {
  $form = array();
  $form['general_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Captcha by IP configuration'),
    '#collapsible' => FALSE,
  );
  $form['general_settings']['captcha_by_ip_header'] = array(
    '#type' => 'textfield',
    '#title' => t('HTTP request header'),
    '#description'    => t("This is the HTTP request header that contains the client IP Address.  It is sometimes re-written by reverse proxies and Content Distribution Networks.  If it is left blank it will be default to REMOTE_ADDR.  In most cases you can leave this blank."),
    '#default_value' => variable_get('captcha_by_ip_header', 'REMOTE_ADDR'),
  );
  $form['general_settings']['captcha_by_ip_markup'] = array(
    '#type' => 'markup',
    '#value' => "Select an <strong>operation</strong> to perform when an IP matches the specified IP range. Exclude will let users with matching IP addresses skip the CAPTCHA. Enforce will force users with matching IP addresses to complete the CAPTCHA while letting any non-matching address skip it.<br />Enter <strong>IP Ranges</strong> in CIDR Notation seperated with semi-colons, with no trailing semi-colon. E.G. 10.20.30.0/24;192.168.199.1/32;1.0.0.0/8<br />For more information on CIDR notation click <a href='http://www.brassy.net/2007/mar/cidr_basic_subnetting'>here</a>.",
    '#prefix' => '<div class="description">',
    '#suffix' => '</div>',
  );
  $form['general_settings']['captcha_by_ip_form_id_overview'] = array(
    '#theme' => 'captcha_by_ip_admin_settings_overview',
    '#tree' => TRUE,
  );
  // Get all currently configured CAPTCHA points
  $result = db_query("SELECT cp.form_id, cbi.operation, cbi.ip_range FROM {captcha_points} cp LEFT JOIN {captcha_by_ip} cbi ON cp.form_id = cbi.form_id ORDER BY cp.form_id");
  while ($captcha_point = db_fetch_object($result)) {
    $form['general_settings']['captcha_by_ip_form_id_overview']['captcha_points'][$captcha_point->form_id] = array();
    $form['general_settings']['captcha_by_ip_form_id_overview']['captcha_points'][$captcha_point->form_id]['form_id'] = array(
      '#value' => $captcha_point->form_id,
    );
    $form['general_settings']['captcha_by_ip_form_id_overview']['captcha_points'][$captcha_point->form_id]['operation'] = array(
      '#type' => 'select',
      '#default_value' => $captcha_point->operation ? $captcha_point->operation : 'none',
      '#options' => array('none' => 'none', 'exclude' => 'exclude', 'enforce' => 'enforce'),
    );
    $form['general_settings']['captcha_by_ip_form_id_overview']['captcha_points'][$captcha_point->form_id]['ip_range'] = array(
      '#type' => 'textfield',
      '#maxlength' => 256,
      '#default_value' => $captcha_point->ip_range ? $captcha_point->ip_range : '',
    );
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );
  return $form;
}

/**
 * Validation for settings form.
 */
function captcha_by_ip_settings_form_validate($form, &$form_state) {
  if (is_array($form_state['values']['captcha_by_ip_form_id_overview']['captcha_points'])) {
    foreach ($form_state['values']['captcha_by_ip_form_id_overview']['captcha_points'] as $form_id => $captcha_point) {
      if ($captcha_point['ip_range'] != '') {
        $result = _captcha_by_ip_validate_ip($captcha_point['ip_range']);
        if ($result['result'] == FALSE) {
          form_set_error('captcha_by_ip_form_id_overview][captcha_points][' . $form_id . '][ip_range', t(implode('<br />', $result['messages'])));
        }
      }
    }
  }
}

/**
 * Submit function for settings form.
 */
function captcha_by_ip_settings_form_submit($form, &$form_state) {
  // Set HTTP request header
  variable_set('captcha_by_ip_header', $form_state['values']['captcha_by_ip_header']);
  // Insert/Update new IP restrictions on CAPTCHA
  $captcha_points = $form_state['values']['captcha_by_ip_form_id_overview']['captcha_points'];
  if (is_array($captcha_points)) {
    // Remove any stale captcha by ip data
    $form_ids = array_keys($captcha_points);
    $form_ids = "'" . implode("', '", $form_ids) . "'";
    db_query("DELETE FROM {captcha_by_ip} WHERE form_id NOT IN (%s)", $form_ids);
    // Insert/Update CAPTCHA point configuations
    foreach ($captcha_points as $form_id => $captcha_point) {
      $record = array('form_id' => $form_id, 'operation' => $captcha_point['operation'], 'ip_range' => $captcha_point['ip_range']);
      if(db_result(db_query("SELECT COUNT(*) from {captcha_by_ip} WHERE form_id = '%s'", $form_id)) > 0) {
        drupal_write_record('captcha_by_ip', $record, 'form_id'); 
      }
      else {
        drupal_write_record('captcha_by_ip', $record); 
      }
    }
  }

}

/**
 * Implementation of hook_form_alter().
 */
function captcha_by_ip_form_alter(&$form, $form_state, $form_id) {
  // Check for IP range entry for form_id
  $row = db_fetch_object(db_query("SELECT * FROM {captcha_by_ip} WHERE form_id='%s'", $form_id));
  if ($row) {
    // Get header from configurable header setting
    $header = variable_get('captcha_by_ip_header', 'REMOTE_ADDR');
    $ip2check = $_SERVER[$header];
    $match = _captcha_by_ip_cidrcheck($ip2check, $row->ip_range);
    if (($match && $row->operation == 'exclude') || (!$match && $row->operation == 'enforce')) {
      unset($form['captcha']);
    }
  }
}

/**
 * Check ip address against a network in cidr notation. E.g:
 * _captcha_by_ip_cidrcheck('192.168.10.100','192.168.10.0/24'); returns 1
 * _captcha_by_ip_cidrcheck('192.168.10.100','192.168.12.0/24'); returns 0
 */
function _captcha_by_ip_cidrcheck($iptocheck, $ipslashcidr) {
  // Seperate ip address and cidr mask
  $netmask = explode("/", $ipslashcidr);
  // Get valid network as long
  $ip_net = ip2long($netmask[0]);
  // Get valid network mask as long
  $ip_mask = ~((1 << (32 - $netmask[1])) - 1);
  // Get ip address to check as long
  $ip_ip = ip2long($iptocheck);
  // Mask ip address to check to get subnet
  $ip_ip_net = $ip_ip & $ip_mask;
  // Only returns 1 if the valid network
  //and the subnet of the ip address
  //to check are the same
  return ($ip_ip_net == $ip_net);
}

/**
 * This function just makes sure the user input for the ip address
 * section is valid.
 */
function _captcha_by_ip_validate_ip($ip_address) {
  $ret = array('result' => TRUE, 'messages' => '');
  $ipaddresses = explode(";", $ip_address);

  // Check each ip address individually
  foreach ($ipaddresses as $ipaddress) {
    // Seperate in to address and cidr mask
    $cidr = explode("/", $ipaddress);
    // Check address and cidr mask entered
    if (count($cidr) == 2) {
      $ipaddr = explode(".", $cidr[0]);
      // Check four octets entered
      if (count($ipaddr) == 4) {
        // Check each octet is valid - numeric and 0 < value < 255
        for ($i=0; $i<count($ipaddr); $i++) {
          if ((!is_numeric($ipaddr[$i])) || ($ipaddr[$i] < 0) || ($ipaddr[$i] > 255)) {
            $ret['messages'][] .= 'Illegal value for an IP Address. Each IP Address must be valid.  Check IP Address ' . $ipaddress;
            $ret['result'] = FALSE;
          }
        }
        // Check cidr mask value - numeric and 0 < value < 33
        if((!is_numeric($cidr[1])) || ($cidr[1]<1) || ($cidr[1]>32)) {
          $ret['messages'][] .= 'Illegal value for CIDR. Please correct CIDR with value of ' . $ipaddress;
          $ret['result'] = FALSE;
        }
      }
      else {
        $ret['messages'][] .= 'IP Address Incorrect Number of Octets. Check IP Address ' . $ipaddress;
        $ret['result'] = FALSE;
      }
    }
    else {
      $ret['messages'][] .= 'IP Address in Incorrect Format. Check IP Address ' . $ipaddress;
      $ret['result'] = FALSE;
    }
  }
  return $ret;
}

function theme_captcha_by_ip_admin_settings_overview($form) {
  $header = array('form_id', t('Operation'), t('IP Range'));
  $rows = array();
  // Existing CAPTCHA points.
  foreach (element_children($form['captcha_points']) as $key) {
    $row = array();
    $row[] = drupal_render($form['captcha_points'][$key]['form_id']);
    $row[] = drupal_render($form['captcha_points'][$key]['operation']);
    $row[] = drupal_render($form['captcha_points'][$key]['ip_range']);
    $rows[] = $row;
  }
  $output = theme('table', $header, $rows);
  return $output;
}
